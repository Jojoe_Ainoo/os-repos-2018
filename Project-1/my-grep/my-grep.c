// Emmanuel Jojoe Ainoo-98932019 (my-grep)
/*T his Program is supposed to behave like the grep command of linux system that
searches for a given item withing a file */

//Including Standard Libraries
#include <string.h>
#include <stdio.h>
#include <stdlib.h> // For exit()

int main(int argc, char const *argv[]) {

  // Declaring Variables
  FILE *file_pointer; //File Pointer to Opened File
  char line_array[512]; //Variable to read characters from file

  //Conditions for Command Line Arguments of Filenames
  if(argv[1] == NULL){
    printf("my-grep: searchterm [file ...]\n"); //Opening Error to Handle for incorrect file reading
    exit(1);
  }
  //Loop to allow multiple Filenames on command line
  for (int i = 2; i < argc; ++i) {
     //Search term but No Filename
     // if(argv[i] == NULL){
     //   printf( "Enter Filename: ");
     //   // gets(argv[i]);
     //   scanf("%s", &argv[i]);
     //   //
     //   // printf( "\nYou entered: %s" ,argv[i]);
     // 
     // }

     file_pointer = fopen(argv[i], "r");// Open file
     if (file_pointer == NULL){
         printf("my-grep: cannot open file\n"); //Opening Error to Handle for incorrect file reading
         exit(1); //Exit if File could not be opened
     }



     int line_num = 1;
     int find_result = 0;

     while(fgets(line_array, 512, file_pointer) != NULL) {
       if((strstr(line_array, argv[1])) != NULL) {
         printf("A match found on line: %d\n", line_num);
         printf("\n%s\n", line_array);
         find_result++;
       }
       line_num++;
     }

     if(find_result == 0) {
       printf("\nSorry, couldn't find a match.\n");
     }
     //Close the file if still open.
     if(file_pointer) {
       fclose(file_pointer);
     }

     fclose(file_pointer); //Close file
   }
   return 0;
}
