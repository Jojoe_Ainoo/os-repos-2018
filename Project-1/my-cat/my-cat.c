// Emmanuel Jojoe Ainoo-98932019 (my-cat)
/*T his Program is supposed to behave like the cat command of linux system that prints out
all the contents of a file */

//Including Standard Libraries
#include <stdio.h>
#include <stdlib.h> // For exit()

int main(int argc, char const *argv[]) {

  // Declaring Variables
  FILE *fptr; //File Pointer to Opened File
  char c; //Variable to read characters from file

  //Conditions for Command Line Arguments of Filenames
  if(argv[1] == NULL){
    printf("my-cat: [file ...]\n"); //Opening Error to Handle for incorrect file reading
    exit(1);
  }
  //Loop to allow multiple Filenames on command line
  for (int i = 1; i < argc; ++i) {
     fptr = fopen(argv[i], "r");// Open file
     if (fptr == NULL)
     {
         printf("my-cat: cannot open file\n"); //Opening Error to Handle for incorrect file reading
         exit(0); //Exit if File could not be opened
     }
     //Loop through and Read contents from file
     c = fgetc(fptr);
     while (c != EOF)
     {
         printf ("%c", c);
         c = fgetc(fptr);
     }
     fclose(fptr); //Close file
   }
   return 0;
}
