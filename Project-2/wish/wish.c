// Emmanuel Jojoe Ainoo  //  OPERATING SYSTEMS Project 2
// Creating Shell
// References:  https://www.geeksforgeeks.org/making-linux-shell-c/
// https://brennan.io/2015/01/16/write-a-shell-in-c/
// https://c-for-dummies.com/blog/?p=1112
// https://www.tutorialspoint.com/cprogramming/c_return_arrays_from_function.htm


// STANDARD LIBRARIES
#include <stdio.h> // Standard Input and Output Operations
#include <string.h> // Functions to manipulate C strings and arrays
#include <stdlib.h> //Four variable types, several macros, and various functions
#include <sys/wait.h> //Symbolic constants for use with waitpid():
#include <unistd.h> // Miscellaneous symbolic constants and types,

// DECLARING CONSTANT VARIABLES
#define TOK_BUFSIZE 100  //Size for Tokenized String Buffer
#define TOK_DELIM " \t\r\n\a" //Token Delimitter for tab, carriage, newline, and alert

//Declared Functions
int BuiltInCD(char **args);
int BuiltInPath(char **args);
int BuiltInExit(char **args);
void ExecuteInteractiveMode(void);
void ExecuteBatchMode(char const *argv[]);

//Declared Arrays for Built-In Commands
char *builtin_str[] = {"cd","path","exit"}; //Built-In command String Array
int (*builtin_func[]) (char **) = {&BuiltInCD,&BuiltInPath,&BuiltInExit}; //Built-In functions array


int main(int argc, char const *argv[]) {

  if(argv[1] == NULL)
  {
    // Run Interactive Shell
    ExecuteInteractiveMode();
  }
  else
  {
    // Run Batch Mode for Shell
    ExecuteBatchMode(argv);

  }
  return 0;
}

// Function to Read Line from Standard Input
char *ReadInput(void)
{
  char *line = NULL; //Assigning pointer to line to be read initially to NULL
  size_t bufsize = 0; //size_t buffersize is required for getline() method
  getline(&line, &bufsize, stdin); // getline from Standard Input with dynamic size
  return line; //Return the Line from Standard input to function call
}


// Function One and Only Error Message
void ErrorMsg()
{
  char error_message[30] = "An error has occurred\n"; // Error Message
  write(STDERR_FILENO, error_message, strlen(error_message)); //Sys call to write Error Message
}

//Function to Parse User Input by tokenizing them by spaces to run as individual commands
char **ParseInput(char *line)
{
  int bufsize = TOK_BUFSIZE, position = 0; //Set Buffer Size for Tokenized String
  char **tokens = malloc(bufsize * sizeof(char*)); // Dynamic allocation to Buffer
  char *token;

  if (tokens == NULL) {
    ErrorMsg();
    exit(EXIT_FAILURE); //Call exit function here
  }

  token = strtok(line, TOK_DELIM); //Parsing the input line into pieces based on ("\t\r\n\a")
  while (token != NULL) {
    tokens[position] = token;
    position++;

    //Reallocate Memory if Buffer gets full
    if (position >= bufsize) {
      bufsize += TOK_BUFSIZE;
      tokens = realloc(tokens, bufsize * sizeof(char*));
      if (!tokens) {
        ErrorMsg();
        exit(EXIT_FAILURE);
      }
    }
    token = strtok(NULL, TOK_DELIM);
  }
  tokens[position] = NULL;
  return tokens; // call exit function here
}


// Function to Create New Process
int CreateProcesses(char **args)
{
   pid_t pid = fork(); // Forking a child
   if (pid == -1) {
       ErrorMsg();
       return 1;
   } else if (pid == 0) {
       if (execvp(args[0], args) < 0) {
           ErrorMsg();
       }
       exit(0);
   } else {
       wait(NULL); // waiting for child to terminate
       return 1;
   }
}

// Function to Return Size of built-in command arrays
int builtins() {
  return sizeof(builtin_str) / sizeof(char *);
}

// Change Directory Built-In Function
int BuiltInCD(char **args)
{
  char error_message_cd[30] = "No such file or directory\n"; // Error Message
  if (args[1] == NULL) {
    fprintf(stderr, "wish: expected argument to \"cd\"\n");
  } else {
    if (chdir(args[1]) != 0) {
      write(STDERR_FILENO, error_message_cd, strlen(error_message_cd));
    }
  }
  return 1;
}

// Function to Return Built-In Path Command
int BuiltInPath(char **args)
{
  // char colon[1000] = ":";
  // for (int i = 1; i < sizeof(args); ++i){
    // strcat(args[i], colon);
    // system("echo New Path is: $PATH");
    // printf("%lu\n", strlen(*args));

  // }


  if(args[1] == NULL){
    setenv("PATH","nothing", 1);
    system("echo New Path is: Empty");
  }
  // printf("%s\n",colon );
  else{
    setenv("PATH", args[1], 1);
    system("echo New Path is: $PATH");
  }
  // free(colon);
  return 1;
}

// Function to Return Size of Built-In Exit
int BuiltInExit(char **args)
{
  exit(0);
}

//Function to Execute Built-In Commands
int RunCommand(char **args)
{
  int i;
  if (args[0] == NULL) {
    return 1; // An empty command was entered.
  }

  for (i = 0; i < builtins(); i++) {
    if (strcmp(args[0], builtin_str[i]) == 0) {
      return (*builtin_func[i])(args);
    }
  }
  return CreateProcesses(args);
}

//Function to Run Interactive Shell
void ExecuteInteractiveMode(void){
  char *line;
  char **args;
  int status;

  do
  {
    printf("wish> ");
    line = ReadInput();
    args = ParseInput(line);
    status = RunCommand(args);

    free(line);
    free(args);
  }
  while (status);
}


// Function to Run Shell in Batch Mode
void ExecuteBatchMode(char const *argv[])
{
  size_t sz = 0;
  ssize_t len = 0;

  char * line = NULL;
  FILE* file = fopen(argv[1], "r");

  char **args;
  int status;

  while((len = getline(&line,&sz,file))>=0){
    write(STDOUT_FILENO, line, strlen(line));
    args = ParseInput(line);
    status = RunCommand(args);
  }
  free(line);
  free(args);

  return;
}
